# Demo Nxc

MPI Ping-Pong

## 0. For the demo

Create a new branch from `main`, and pull that branch on G5K.
Then when you are done with the demo on your laptop, push on that branch, and pull on g5k.

## 1. Add the `ping-pong` package

```nix
{ pkgs, ... }: {
  roles = {
    node = { pkgs, ... }:
    let
        ping-pong = pkgs.callPackage ./ping-pong.nix { };
    in
      {
        environment.systemPackages = with pkgs; [ openmpi ping-pong ];
      };
  };
  testScript = ''
  '';
}
```


## 2. Demo with docker

```sh
nxc build -f docker
nxc start
nxc connect
nxc stop
```

Show that the `ping-pong` bin is there

## 3. Add a node


```sh
nxc start -r node=2
nxc connect
nxc stop
```

Show that both the node have `ping-pong` bin is there

```
echo -e "node1 slots=1\nnode2 slots=1" > hostfile
mpirun --allow-run-as-root -np 2 --hostfile hostfile ping-pong
```

## 4. Try with VMs

```sh
nxc build -f vm
nxc start -r node=2
nxc connect
nxc stop
```

```
echo -e "node1 slots=1\nnode2 slots=1" > hostfile
mpirun --allow-run-as-root -np 2 --hostfile hostfile ping-pong
```

## 5. Push and pull on G5k

```sh
nxc build -f g5k-nfs-store
export $(oarsub -l nodes=2,walltime=0:10:0 "$(nxc helper g5k_script) 1h" | grep OAR_JOB_ID)
oarstat -u -J | jq --raw-output 'to_entries | .[0].value.assigned_network_address | .[]' | tee machines
nxc start -r node=2 -m machines
nxc connect
nxc stop
```

```
echo -e "node1 slots=1\nnode2 slots=1" > hostfile
mpirun --allow-run-as-root -np 2 --hostfile hostfile ping-pong
```

