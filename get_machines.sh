set -xe
oarstat -u -J | jq --raw-output 'to_entries | .[0].value.assigned_network_address | .[]' | tee machines
