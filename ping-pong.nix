{ stdenv, openmpi }:

stdenv.mkDerivation {
    name = "ping-pong";
    version = "0.0.0";
    src = ./ping-pong;
    buildInputs = [ openmpi ];
    buildPhase = ''
        mpicc ping-pong.c -o ping-pong
    '';
    installPhase = ''
        mkdir -p $out/bin  
        cp ping-pong $out/bin
    '';
}
